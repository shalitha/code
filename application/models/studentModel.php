<?php
class StudentModel extends CI_Model{
	protected $table = 'students';

	public function getAll(){
		return $this->db->get($this->table);
	}

	public function getOne($id){
		$rs = $this->db->get_where($this->table, array(
			'id' => $id
		))->result();

		return $rs[0];
	}

	public function update($data){
		$this->db->where('id', $data['id']);
		return $this->db->update($this->table, $data);
	}

	public function delete($id){
		$this->db->where('id', $id);
		return $this->db->delete($this->table);
	}

	public function save($data){
		return $this->db->insert($this->table, $data);
	}
}