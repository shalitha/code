<?php
class Student extends CI_Controller{
	public function index(){
		$this->load->model('StudentModel', '', TRUE);//calling database connectivity ready model
		$data = array(
			'title' => 'Student Home',
			'data' => $this->StudentModel->getAll()->result(),
			'success' => $this->input->get('success')
		);
		$this->load->view('student/student_home', $data);
	}

	public function view($id = 1){
		$this->load->model('StudentModel', '', TRUE);//calling database connectivity ready model
		$data = array(
			'title' => 'Student View',
			'student' => $this->StudentModel->getOne($id)
		);
		$this->load->view('student/student_view', $data);
	}

	public function create(){
		$this->load->view('student/student_new', array('title'=>'Create Student'));
	}

	public function save(){
		$this->load->model('StudentModel', '', TRUE);//calling database connectivity ready model

		$data = $this->input->post();
		if($this->StudentModel->save($data))
			redirect('student/?success=true');
		else
			redirect('student/?success=false');
	}

	public function edit($id = 1){
		$this->load->model('StudentModel', '', TRUE);//calling database connectivity ready model
		$data = array(
			'title' => 'Student View',
			'student' => $this->StudentModel->getOne($id)
		);
		$this->load->view('student/student_edit', $data);
	}

	public function update(){
		$this->load->model('StudentModel', '', TRUE);//calling database connectivity ready model

		$data = $this->input->post();
		if($this->StudentModel->update($data)){
			redirect('student/?success=true');
		}else{
			redirect('student/?success=false');
		}
	}

	public function delete($id){
		$this->load->model('StudentModel', '', TRUE);//calling database connectivity ready model
		
		if($this->StudentModel->delete($id)){
			redirect('student/?success=true');
		}else{
			redirect('student/?success=false');
		}
	}
}