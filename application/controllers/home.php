<?php

class Home extends CI_Controller{
	public function index(){
		echo "Hello World!";
	}

	public function welcome_message(){
		// $this->load->helper('url');
		$this->load->model('Student', '', TRUE);//calling database connectivity ready model
		// var_dump($this->Student->getAll());
		$data = array(
			'msg' => 'Hello World',
			'data' => $this->Student->getAll()->result()
		);
		$this->load->view('home', $data);
	}
}