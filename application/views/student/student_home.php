<!DOCTYPE html>
<html>
<head>
	<title><?= $title ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/bootstrap.css">
</head>
<body>
	<div class="container">
		<?php if($success != null){ ?>
		<?php if($success == 'true'){ ?>
		<div class="alert alert-success" role="alert">Success</div>
		<?php }else{ ?>
		<div class="alert alert-danger" role="alert">Unsuccessful</div>
		<?php } } ?>
		<h1>Student List</h1>
		<a class="btn btn-primary" href="<?= base_url("student/create") ?>">Create new</a>
		<table class="table">
			<tr>
				<th>Name</th>
				<th>Email</th>
				<th>Country</th>
				<th>&nbsp;</th>
				<th>&nbsp;</th>
			</tr>
			<?php foreach ($data as $student) {?>
			<tr>
	<td>
		<a href="<?php echo base_url('student/view/'.$student->id); ?>">
			<?php echo $student->name ?>
		</a>
	</td>
	<td><?php echo $student->email ?></td>
	<td><?php echo $student->country ?></td>
	<td><a href="<?php echo base_url('student/edit/'.$student->id); ?>">Edit</a></td>
	<td><a href="<?php echo base_url('student/delete/'.$student->id); ?>">Delete</a></td>
			</tr>
			<?php } ?>
		</table>
	</div>
</body>
</html>