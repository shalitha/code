<!DOCTYPE html>
<html>
<head>
	<title><?= $title ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/bootstrap.css">
</head>
<body>
	<div class="container">
		<h1><?= $student->name ?></h1>
		<p>Email : <?= $student->email ?></p>
		<p>Country : <?= $student->country ?></p>
	</div>
</body>
</html>