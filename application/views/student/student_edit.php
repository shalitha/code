<!DOCTYPE html>
<html>
<head>
	<title><?= $title ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/bootstrap.css">
</head>
<body>
	<div class="container">
		<h1>Edit Student</h1>	
		<form method="post" action="<?= base_url('student/update'); ?>">
			<div class="form-group">
				<label>Name</label>
				<input name="name" class="form-control" value="<?= $student->name ?>" />
			</div>

			<div class="form-group">
				<label>Email</label>
				<input name="email" type="email" class="form-control" value="<?= $student->email ?>" />
			</div>

			<div class="form-group">
				<label>Country</label>
				<input name="country" class="form-control" value="<?= $student->country ?>" />
			</div>
			<input type="hidden" name="id" value="<?= $student->id ?>">
			<input type="submit" value="Submit" class="btn btn-default">
		</form>
	</div>
</body>
</html>