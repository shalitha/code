<!DOCTYPE html>
<html>
<head>
	<title><?= $title ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/bootstrap.css">
</head>
<body>
	<div class="container">
		<h1>Create Student</h1>	
		<form method="post" action="<?= base_url('student/save'); ?>">
			<div class="form-group">
				<label>Name</label>
				<input name="name" class="form-control" placeholder="Student Name" />
			</div>

			<div class="form-group">
				<label>Email</label>
				<input name="email" type="email" class="form-control" placeholder="Student Email" />
			</div>

			<div class="form-group">
				<label>Country</label>
				<input name="country" class="form-control" placeholder="Student Country" />
			</div>
			<input type="submit" value="Submit" class="btn btn-default">
		</form>
	</div>
</body>
</html>